// initializing require directive
let http = require("http");

// initializing local port endpoint
const port = 3000;

// using createServer method to initialize a local server
const server = http.createServer((request, response) => {
		if( request.url == '/login'){
			response.writeHead(200, {'Content-Type': 'text-plain'});
			response.end('You are in the login page');
		} else if( request.url == '/dashboard') {
			response.writeHead(200, {'Content-Type': 'text-plain'});
			response.end('You are in the dashboard page');
		} else {
			response.end('Page does not exist, look for other routes. Try /dashboard or /login pages.')
		}

});

server.listen(port);
console.log("Your server is up and running at the local port of 3000");
